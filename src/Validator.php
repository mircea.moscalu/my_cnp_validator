<?php

namespace MirceaM\CNPValidator;

class Validator{

    CONST NR_USED_FOR_CALCULATION_OF_THE_CONTROL_DIGIT = '279146358279'; 

    protected $cnp = '';
    protected $valid = null; // if null, no validation has been performed
    protected $error = '';

    public function __construct( $cnp = '' ){
        $this->setCnp( $cnp );
    }

    public function getError(){
        return $this->error;
    }

    protected function reset(){
        $this->valid = null;
        $this->error = '';
    }

    public function setCnp( $cnp = '' ){
        
        if( !is_numeric($cnp)){
            throw new \Exception( 'Parametrul pentru CNP trebuie sa fie numeric.' );
        } 
        
        $this->cnp = (string)$cnp;
        
        $this->reset();   
    }

    public function isValid(){

        if( $this->valid == null ){
            $this->valid = $this->validate();
        }
        return $this->valid;
    }

    protected function getControlDigitFromCnp(){

        $sum = 0;
        $first12 = substr( $this->cnp, 0, 12 );
        
        for ( $i = 0; $i <= strlen( $first12 ) - 1; $i++){ 
            $sum += $first12[$i] * self::NR_USED_FOR_CALCULATION_OF_THE_CONTROL_DIGIT[$i];
        }
        
        $controlDigit = $sum % 11; 
        if( $controlDigit == 10 ){
            return 1;
        }

        return $controlDigit;
    }

    protected function validate(){

        $this->reset();
        
        if( !is_string($this->cnp) ){
            return false;
        }
        
        $matched = (bool)preg_match('/^(?P<sex>[1-9])(?P<an>\d{2})(?P<luna>0[1-9]|1[0-2])(?P<zi>0[1-9]|[1|2]\d|3[0|1])(?P<judet>0[1-9]|[1-3]\d|4[0-6]|5[1|2])(?P<nnn>00[1-9]|0[1-9]\d|[1-9]\d{2})(?P<control>\d)$/', $this->cnp, $matches ); 
        
        if( $matched ){
            
            $controlDigit = $this->getControlDigitFromCnp();
            
            if( (int)substr($this->cnp, -1, 1) != $controlDigit ){
                $matched = false;
            }
        }
        
        return $matched;
    }

}