#!/usr/bin/php

<?php

if (php_sapi_name() !== 'cli') {
    exit;
}

require __DIR__ . '/vendor/autoload.php';

use MirceaM\CNPValidator\Validator;

try {

    $validator = new Validator( isset($argv[1] ) ? $argv[1] : null );

} catch( \Exception $e) {
    
    echo $e->getMessage() . PHP_EOL;
    exit;
}

if( $validator->isValid() ){
    echo 'CNP-ul '. $argv[1] . ' este valid' . PHP_EOL; exit;
}

echo 'CNP-ul ' . $argv[1] . ' este invalid ' . PHP_EOL;
echo $validator->getError() . PHP_EOL;
